library(rvest)
library(tidyverse)

# webpage and xpath for scores table
#url <- 'https://www.sports-reference.com/cfb/years/2021-schedule.html'

# get college football scores from sports-reference.com
get_scores_CFB <- function(url) { 
  
  # read in raw score data
  df <- url %>%
    read_html() %>%
    html_nodes(xpath='//*[@id="schedule"]') %>%
    html_table() %>% 
    as.data.frame()
  
  # Clean up raw table
  df <- subset(df,Rk != 'Rk') %>% 
    dplyr::select(-Rk) %>% 
    rename(HomeAway = Var.8,
           WinnerPts = Pts,
           LoserPts = Pts.1) %>% 
    mutate(HomeAway = 
             case_when(HomeAway == "" ~ "H",
                       HomeAway == "@" ~ "A",
                       HomeAway == "N" ~ "N",
                       TRUE ~ "U"),
           WinnerRank = as.integer(str_extract(Winner,'(?<=\\()\\d+')),
           LoserRank = as.integer(str_extract(Loser,'(?<=\\()\\d+')),
           Winner = str_trim(str_replace(Winner,'\\(\\d+\\)','')),
           Loser = str_trim(str_replace(Loser,'\\(\\d+\\)','')),
           Date = as.Date(Date, format='%b %d, %Y'),
           Game = 1:n(),
           WinnerPts = as.integer(WinnerPts),
           LoserPts = as.integer(LoserPts),
           Wk = as.integer(Wk)) %>%
    relocate(Game,.before = Wk)
  
  df
  
}

