# Regression Ranking Function

# Overview - model margin of victory using linear regression with 
#   team ratings and home field status predictors.
#   similar to massey and winston methods, with extra bells and whistles, including
#   options for ridge regression and random effects models in addition to traditional ols

# args:
#    data:          data frame
#    w:             field name for game winner (character)
#    l:             field name for game loser (character)
#    w.p:           field name for points scored by game winner (character)
#    l.p:           field name for points scored by game loser (character)
#    date:           field name for game date
#    min.game:      integer input to exclude games where at least one 
#                   of the teams played fewer than specified minimum games
#                   during season. Can be used to troubleshoot matrix
#                   inversion issues
#    weight.type:   specify weighting methodology, used to emphasize games later in season.
#                   none" (default),"linear","log","exp","step"
#    ties:          "include" (default) or "ignore" which throws out all tie games
#    step.wts:      vector of custom weights to apply if using "step" weighting, length(n)
#    step.thresh:   vector of thresholds in which to apply custom weights, length(n-1)
#    ties:          "include" (default) or "ignore" which throws out all tie orinduced tie games  
#    induce.ties:   TRUE/FALSE (default). Whether or not close games should be treated
#                   as tied games
#    induce.thresh: point differential threshold to use for calculating induced ties
#    model.type:
#    ref.team:      default NA, specify base team with rating 0, only applies to "basic" model type
#    seed:          1984
# output:
#   list of data frames: rankings, defensive rankings, offensive rankings 


# required libraries
library(dplyr)
library(glmnet)
library(lme4)

# external helper functions
source("weighting_functions.R")
source("volume_tie_functions.R")
source('scores_formatting_functions.R')

reg_ranking <- function(data, w="Winner",l="Loser",
                        w.p="WinnerPts", l.p = "LoserPts",
                        date="Date", min.game=1,
                        weight.type="none",
                        step.wts=NA, step.thresh=NA, ties="include",
                        induce.ties=FALSE, induce.thresh=3,
                        model.type = "basic",ref.team = NA, seed=1984
                        ) {
  
  # make sure input parameters are correct
  if(ties %in% c("include","ignore") & 
     weight.type %in% c("none","linear","log","exp","step") &
     model.type %in% c("basic","l2","re")) {
    
    # remove low volume games based on specified threshold
    scores <- df_hi_vol(data, w, l, min.game)
    
    # make scores equal if induced tie option set to TRUE
    if (induce.ties) scores <- df_induce_ties(scores, w.p, l.p,induce.thresh)
    
    # remove tie games if specified
    if (ties == "ignore") scores <- df_no_ties(scores,w.p,l.p)  
    
    # total number of games
    num.games <- nrow(scores)
   
    # get game weights - set to vector of 1s if weight.type is "none"
    wt.vec <- get_wt_vec(scores,date,weight.type,num.games)
    
    # distinct team names - sorted in alphabetical order
    teams <- sort(unique(c(scores[,w],scores[,l])))
    
    # number distinct teams
    num.teams <- length(teams)
    
    # convert scores df from winner/loser to home/away format
    scores <- convert_wl_ha(scores)
    
    if (model.type %in%  c("basic",'l2') ) {
      
      # matrix - rows: games, cols: teams, win = 1; loss = -1
      X <- matrix(data=0, nrow = num.games, ncol = num.teams,
                dimnames=list(NULL,teams))
    
      for (i in 1:num.games) {
        idx1 = match(scores[,"Home"][i],teams)
        idx2 = match(scores[,"Away"][i], teams)
      
        X[i, idx1] = 1
        X[i, idx2] = -1
      }
      
        
      # home status indicator - only needed to account for neutral games
      home <- ifelse(scores$Neutral == 1,0, 1)
      
      # margin of victory for home team
      mov <- scores$HomePts - scores$AwayPts 
      
    } 
    
    if (model.type == "basic") {
      
      # remove reference team column from matrix to remove linear dependence
      # reference team rating will be 0
      ref.col <- ifelse(!is.na(ref.team) & ref.team %in% teams,
                        which(colnames(X) == ref.team),ncol(X))
      X <- X[,-ref.col]      
      
      # final modeling matrix
      M <- data.frame(cbind(mov = mov, X, home=home))
      
      # model
      mymodel <- lm(mov ~ . + 0, data=M, weights=wt.vec )
      
      # get ratings for each team 
      m.coef <- coef(mymodel)
      m.coef <- c(m.coef[-length(m.coef)],0)
      names(m.coef) <- c(teams[-ref.col], teams[ref.col])
      
      # sorted ratings and rankings rankings
      rk <- tibble(rating=m.coef, team=names(m.coef)) %>%
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)
      
      # output ranking, home field advantage, model
      list(rank=rk, hfa = coef(mymodel)[length(coef(mymodel))], model=mymodel)
    
    } else if (model.type == "l2") {
      
      # model with L2 penalty; no intercept, don't regularize home field variable
      # no need to have reference level -- ridge regression handles multicollinearity well
      set.seed(seed)
      
      M.glm <- cbind(X, home=home)
      
      mymodel <- cv.glmnet(M.glm,mov,alpha=0,family="gaussian", intercept=F,
                           penalty.factor = c(rep(1, num.teams),0),weights = wt.vec)
      
      
      m.coef <- coef(mymodel, s = mymodel$lambda.min)[-1,]
      
      m.coef <- m.coef[-length(m.coef)]
      names(m.coef) <- teams
      
      
      rk <- tibble(team = names(m.coef),rating=as.numeric(m.coef)) %>% 
        filter(team != "(Intercept)", team != "home") %>% 
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)
      
      # output ranking, home field advantage, model
      list(rank=rk, hfa = coef(mymodel)[length(coef(mymodel))], model=mymodel)
    } else {
      
      # modeling data frame, each game gets two rows with alternating team 1 and team 2 assignments
      M.re <- scores %>% 
        mutate(team1 = factor(Home),
               team2 = factor(Away),
               home = ifelse(Neutral == 1, 0, 1),
               mov = HomePts - AwayPts
        ) %>% 
        select(team1, team2, home, mov) %>% 
        bind_rows(
          scores %>% 
            mutate(team1 = factor(Away),
                   team2 = factor(Home),
                   home = ifelse(Neutral == 1, 0, -1),
                   mov = AwayPts - HomePts) %>% 
            select(team1,team2,home,mov)
        )
      
      # fit random effects model
      mymodel <- lmer(mov ~ (1|team1) + (1|team2) + home + 0, data=M.re,
                       weights = c(wt.vec, wt.vec))
      mymodel.re <- ranef(mymodel)$team1
      
      # ranking data frame 
      rk <- tibble(team = rownames(mymodel.re),rating=as.numeric(mymodel.re[,1])) %>% 
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)
      
      #output ranking, home field advantage, model
      list(rank=rk, hfa = summary(mymodel)$coefficients[1], model=mymodel)
      
      
      
    }
    
    } else {
      stop("Invalid arguments!")
  }
}

#### TESTING

setwd("G:\\My Drive\\Documents\\Learn\\Whos1\\Repos\\ranking\\code\\R")

source('get_scores_CFB.R')

url <- 'https://www.sports-reference.com/cfb/years/2021-schedule.html'
df <- get_scores_CFB(url)
glimpse(df)
df <- df %>% filter(Date <= '2021-12-05')

reg_ranking(df, min.game=6, ref.team="Penn State",model.type="l2", weight.type = "log")$rank %>% View()


reg_ranking(df, min.game=6, ref.team="P",model.type="l2", weight.type = "none")$hfa
