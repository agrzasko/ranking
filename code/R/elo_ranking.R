# Elo Ranking Function
# args:
#    data:        data frame
#    k:           weighting factor to apply to difference between actual and expected for each game outcome  
#    xi:          scaling factor in logistic function
#    home.adv     home field advantage - additive term to ratings for home team 
#    w:           field name for game winner (character)
#    l:           field name for game loser (character)
#    w.p:         field name for points scored by game winner (characger)
#    l.p:         field name for points scored by game loser (character)
#    h.a:         field name for home/away status for winning team - expect "H","A",or "N" for neutral
#    type:        "wins" (default) or "points", what to use for calculating rankings
#    initial_rate: initial rating vector or scalar to apply at beginning of season, default is scalar 0 
#    laplace:     TRUE (default)/FALSE, apply laplace rule of succession to raw statistics (works with "prop" only)
#    norm.games:  TRUE/FALSE (default) , divide raw matchup statistics by number of games teams played against one another  
#    base:        base to use in logistic function.  Default is 10
#    min.game:    integer input to exclude games where at least one 
#                 of the teams played fewer than specified minimum games
#                 during season. Can be used to exclude FCS games 
# output:
#    list of data frame with rank, team name, and rating; and brier score

library(tidyverse)

elo <- function(data, k=105, xi = 400, home.adv=105, w="Winner",l="Loser",
                w.p="WinnerPts", l.p = "LoserPts", h.a = "HomeAway",
                type = "wins", initial.rate = 1500, laplace=TRUE,
                base=10, min.game=1) {
  
  if(type %in% c("wins","points")) {
  
    # logistic helper function
    log.func <- function(r_i,r_j,xi, base) {
      1 / (1 + base^(-(r_i - r_j)/xi))
    }
  
    # update ratings function
    update.rate <- function(r,k,s,mu) {
      r + k * (s - mu)
    }
    
    # scores data frame
    scores <- data[,c(w,l,w.p,l.p,h.a)]
  
    # get teams with total games played satisfying min threshold
    team.filt <- scores %>% count(team=!!sym(w)) %>%  
      full_join(scores %>% count(team=!!(sym(l))), by = "team",
                suffix=c(".win",".lose")) %>% 
      mutate(n.win = replace_na(n.win,0),
             n.lose = replace_na(n.lose,0),
             n.total = n.win + n.lose) %>% 
      select(-n.win,-n.lose) %>% 
      filter(n.total >= min.game) %>% 
      pull(team)
  
    # remove games involving low volume teams from scores dataframe
    scores <- scores %>% 
      filter(!!sym(w) %in% team.filt,
             !!sym(l) %in% team.filt)
  
    # distinct team names sorted in alphabetical order
    teams <- sort(unique(c(scores[,w],scores[,l])))
 
    # number distinct teams
    num.teams <- length(teams)
  
    # home away vector
    ha.vec <- ifelse(scores[,h.a] == "H" ,1,
                     ifelse(scores[,h.a] == "A",-1,0))
  
    # make sure initial ratings is scalar or equal to number of unique teams
    if(length(initial.rate) != 1 & length(initial.rate != num.teams)) {
      stop('initial ratings vector must be scalar or equal to number of unique teams')
    }
  
    # initial ratings vec
    if (length(initial.rate) ==1) {
    
      rating.vec <- rep(initial.rate,num.teams)
  
    } else {
    
      rating.vec <- initial.rate
    }
  
    names(rating.vec) <- teams
  
    # capture squared error
    sq.err <- numeric(nrow(scores))
    
    # update ratings based on each game outcome
    for (i in 1:nrow(scores)) {
    
      idx1 = match(scores[i,w],teams)
      idx2 = match(scores[i,l], teams)
    
      mu1 <- log.func(rating.vec[idx1] + ha.vec[i]*home.adv, rating.vec[idx2],xi,base)
      mu2 <- log.func(rating.vec[idx2], rating.vec[idx1] + ha.vec[i]*home.adv,xi,base)
    
      if (type == 'wins') {
        s1 <- ifelse(scores[i,w.p] == scores[i,l.p], 0.5, 1)
        s2 <- 1- s1
      
      
      } else {
        s1 <- (scores[i,w.p] + laplace) / (scores[i,w.p] + scores[i,l.p] + 2*laplace)
        s2 <- (scores[i,l.p] + laplace) / (scores[i,w.p] + scores[i,l.p] + 2*laplace)
      
      }
    
      sq.err[i] <- (mu1 - s1)^2
      
      rating.vec[idx1] <- update.rate(rating.vec[idx1],k,s1,mu1)
      rating.vec[idx2] <- update.rate(rating.vec[idx2],k,s2,mu2)
     
    
    }
  
    rating.vec <- sort(rating.vec,decreasing = T)
  
    # ranking data frame
    rk.df <- tibble(rk = 1:num.teams, team=names(rating.vec),rating=rating.vec)
    
    # brier score
    brier <- mean(sq.err)
    
    # output ranking dataframe and brier score
    
    list(rank=rk.df, brier=brier)
    
  } else {
    stop('invalid arguments!')
    }   
  
}

