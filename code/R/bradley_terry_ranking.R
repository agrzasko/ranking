# Bradley-Terry Ranking Function

# Overview:         Game win loss/outcomes and home/away status used with logistic regression model
#                   to solve for individual team ratings
#                   Can also implement L2 regularized ratings or ratings using random effects
# args:
#    data:          data frame in winner/loser format
#    w:             field name for game winner (character)
#    l:             field name for game loser (character)
#    w.p:           field name for points scored by game winner (character)
#    l.p:           field name for points scored by game loser (character)
#    h.a:           field name to indicate home field status for winner, "H","A","N"
#    date:           field name for game date
#    min.game:      integer input to exclude games where at least one 
#                   of the teams played fewer than specified minimum games
#                   during season. Can be used to troubleshoot matrix
#                   inversion issues
#    model.type     one of "basic" (default), "l2", "re" for traditional logistic regression, logistic
#                   regression with L2 penalty, logistic generalized mixed model with team-specific 
#                   random effects, respectively. The "re" option is currently in an experimental state
#    epsilon:       numeric, default 0, typically use small values (e.g., 1e-6) when there are 
#                     convergence issues, particularly helpful for "basic" model when including weights.
#                     epsilon values are either added or subtracted from actual outcomes (0 or 1, respectively)
#    weight.type:   specify weighting methodology, used to emphasize games later in season.
#                   "none" (default),"linear","log","exp","step", currently considered experimental
#    ties:          "include" (default) or "ignore" which throws out all tie games
#    step.wts:      vector of custom weights to apply if using "step" weighting, length(n)
#    step.thresh:   vector of thresholds (dates) in which to apply custom weights, length(n-1)
#    ties:          "include" (default) or "ignore" which throws out all actual or induced ties  
#    induce.ties:   TRUE/FALSE (default). Whether or not close games should be treated
#                   as tied games
#    induce.thresh: point differential threshold to use for calculating induced ties  
#    seed:          used for reproducibility purposes when using l2 model which employes cross validation
#
# output:
#   list output: rankings/ratings data frame, home field advantage coefficient scalar, model

# libraries
library(dplyr)
library(glmnet)
library(lme4)

# helper functions
source('scores_formatting_functions.R')
source("weighting_functions.R")
source("volume_tie_functions.R")

bradley_terry <- function(data, w="Winner",l="Loser",
                          w.p="WinnerPts", l.p = "LoserPts",
                          date="Date", min.game=1,
                          h.a = "HomeAway",
                          model.type = "basic", epsilon=0,
                          weight.type="none",
                          step.wts=NA, step.thresh=NA, ties="include",
                          induce.ties=FALSE, induce.thresh=3, seed=1984) {
  
  if(ties %in% c("include","ignore") & 
     weight.type %in% c("none","linear","log","exp","step") &
     model.type %in% c("basic","l2","re")) {
    
    # remove low volume games based on specified threshold
    scores <- df_hi_vol(data, w, l, min.game)
    
    # make scores equal if induced tie option set to TRUE
    if (induce.ties) scores <- df_induce_ties(scores, w.p, l.p,induce.thresh)
    
    # remove tie games if specified
    if (ties == "ignore") scores <- df_no_ties(scores,w.p,l.p)  
    
    # total number of games
    num.games <- nrow(scores)
    
    # get game weights - will be all 1s if weight.type is "none"
    wt.vec <- get_wt_vec(scores,date,weight.type,num.games)
    
    # distinct team names
    teams <- unique(c(scores[,w],scores[,l]))
    
    # number distinct teams
    num.teams <- length(teams)
    
    # convert scores df to from winner/loser to home/away format
    scores <- convert_wl_ha(scores, w, l, w.p, l.p, h.a)
    
    
    if (model.type %in%  c("basic",'l2') ) {
      
      # home/away game matrix - rows: games, cols: teams, home = 1; away = -1
      X <- matrix(data=0, nrow = num.games, ncol = num.teams,
                  dimnames=list(NULL,teams))
      
      for (i in 1:num.games) {
        idx1 = match(scores[,"Home"][i],teams)
        idx2 = match(scores[,"Away"][i], teams)
        
        X[i, idx1] = 1 
        X[i, idx2] = -1 
      }
      
      # home status indicator - only needed to account for neutral games
      home <- ifelse(scores$Neutral == 1,0, 1)
      
      # outcome of game, 0.5 for ties, introduce noise for wins/losses using epsilon
      outcome <- ifelse(scores$HomePts > scores$AwayPts, 1 - epsilon,
                        ifelse(scores$HomePts < scores$AwayPts,0 + epsilon,0.5))
    }
    
    if (model.type == "basic") {
      
      # final modeling matrix
      M <- data.frame(cbind(outcome = outcome, X, home=home))
      
      # model
      mymodel <- glm(outcome ~ . + 0 , family = "binomial", data=M, maxit=100, weights=wt.vec )
      
      # get rating coefficients -- set NAs to 0
      m.coef <- replace_na(coef(mymodel),0)
      m.coef <- m.coef[-length(m.coef)]
      
      names(m.coef) <- teams
      
      # sorted ratings and rankings rankings
      rk <- tibble(rating=m.coef, team=names(m.coef)) %>%
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)
      
      # output ranking, home field advantage, model
      list(rank=rk, hfa = coef(mymodel)[length(coef(mymodel))], model=mymodel)
      
    } else if (model.type == "l2") {
      
      # model with L2 penalty; no intercept, don't regularize home field variable
      set.seed(seed)
      
      M.glm <- cbind(X, home=home)
      
      mymodel <- cv.glmnet(M.glm, outcome,alpha=0,family="binomial", intercept=F,
                      penalty.factor = c(rep(1, num.teams),0),weights = wt.vec)
      
      mymodel.coef <- coef(mymodel, s = mymodel$lambda.min)
      
      rk <- tibble(team = rownames(mymodel.coef),rating=as.numeric(mymodel.coef)) %>% 
        filter(team != "(Intercept)", team != "home") %>% 
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)
      
      # output ranking, home field advantage, model
      list(rank=rk, hfa = coef(mymodel)[length(coef(mymodel))], model=mymodel)
      
    } else {
      
      # modeling data frame, each game gets two rows with alternating team 1 and team 2 assignments
      M.re <- scores %>% 
        mutate(team1 = factor(Home),
               team2 = factor(Away),
               home = ifelse(Neutral == 1, 0, 1),
               outcome = ifelse(HomePts > AwayPts, 1 - epsilon, ifelse(HomePts == AwayPts,0.5,0 + epsilon))
        ) %>% 
        select(team1, team2, home, outcome) %>% 
        bind_rows(
          scores %>% 
            mutate(team1 = factor(Away),
                   team2 = factor(Home),
                   home = ifelse(Neutral == 1, 0, -1),
                   outcome = ifelse(HomePts > AwayPts, 0 + epsilon, ifelse(HomePts == AwayPts,0.5,1 - epsilon))) %>% 
            select(team1,team2,home,outcome)
        )
      
      # fit random effects model
      mymodel <- glmer(outcome ~ (1|team1) + (1|team2) + home + 0, family="binomial", data=M.re,
                       weights = c(wt.vec, wt.vec))
      mymodel.re <- ranef(mymodel)$team1
      
      # ranking data frame 
      rk <- tibble(team = rownames(mymodel.re),rating=as.numeric(mymodel.re[,1])) %>% 
        arrange(-rating) %>%
        mutate(rank = row_number()) %>%
        select(rank, team, rating)

      #output ranking, home field advantage, model
      list(rank=rk, hfa = summary(mymodel)$coefficients[1], model=mymodel)
    }
    
  } else {
    stop('Invalid arguments!')
  }
}
  