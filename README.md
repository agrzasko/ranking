# Ranking Algorithms  
  

## Introduction  

This repository contains a series of ranking algorithms, with an emphasis on applications in sports.  

  
## Functionality  


Ranking algorithms and web scraping scripts are provided.  
  
Algorithms:   

* Massey - total, offense, and defense rankings; can also be used to estimate point spreads
* Colley
* Keener
* Elo  
* Elo alternative implementation using stochastic gradient descent
* Offense-Defense  - total, offense, and defense rankings
* Markov
* Rank differential  
* Spread ranking - can also be used to project point spreads

Scrapers:  

* College football - using sports-reference.com  

## Technology

All scripts are developed in R, with future Python development anticipated.  
  

